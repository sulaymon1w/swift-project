//
//  ContentView.swift
//  PeekScrolling
//
//  Created by Sulaymon on 12/08/21.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        ZStack {
            Color(#colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)).edgesIgnoringSafeArea(.all)
    
            RoundedRectangle(cornerRadius: 30)
                .fill(LinearGradient(gradient: Gradient(stops:[.init(color: #colorLiteral(red: 0.1764705926, green: 0.01176470611, blue: 0.5607843399, alpha: 1)), location: 0)]), startPoint: /*@START_MENU_TOKEN@*/.leading/*@END_MENU_TOKEN@*/, endPoint: /*@START_MENU_TOKEN@*/.trailing/*@END_MENU_TOKEN@*/))
            
//            VStack(spacing: 10.0) {
//                Text("Courses")
//                    .font(.largeTitle)
//                    .bold()
//                    .frame(maxWidth: .infinity,alignment: .leading)
//                    .padding(.horizontal,30)
//                ZStack {
////                    Image("undraw_Emails_re_cqen")
////                        .offset(x:100)
//                    Image("undraw_Emails_re_cqen")
//                        .frame(maxWidth: 300)
//                }
//                Spacer()
//            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
