//
//  PeekScrollingApp.swift
//  PeekScrolling
//
//  Created by Sulaymon on 12/08/21.
//

import SwiftUI

@main
struct PeekScrollingApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
