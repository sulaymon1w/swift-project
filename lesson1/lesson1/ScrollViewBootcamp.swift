//
//  ScrollViewBootcamp.swift
//  lesson1
//
//  Created by Sulaymon on 23/08/21.
//

import SwiftUI

struct ScrollViewBootcamp: View {
    var body: some View {
        ScrollView(.vertical, showsIndicators: false, content: {
            LazyVStack {
                ForEach(0..<10, content:{index in
                    ScrollView(.horizontal, showsIndicators: false, content: {
                        LazyHStack {
                            ForEach(0..<10) { index2 in
                                VStack {
                                    RoundedRectangle(cornerRadius: 25.0)
                                        .fill(Color.white)
                                        .frame(width: 200, height: 150, alignment: .center)
                                        .shadow(radius: 10)
                                        .padding([.top, .leading, .trailing])
                                        .overlay(Text("\(index2+1)"))
                                    Text("Sulaymon Yahyo")
                                }
                            }
                        }
                    })
                })
    }
})
        }
    
}

struct ScrollViewBootcamp_Previews: PreviewProvider {
    static var previews: some View {
        ScrollViewBootcamp()
    }
}
