//
//  GridBootcamp.swift
//  lesson1
//
//  Created by Sulaymon on 24/08/21.
//

import SwiftUI

struct GridBootcamp: View {
    
    let columns: [GridItem] = [
        GridItem(.flexible(),spacing: nil,alignment: nil),
        GridItem(.flexible(),spacing: nil,alignment: nil),
        GridItem(.flexible(),spacing: nil,alignment: nil)
        //        GridItem(.fixed(50)),
        //        GridItem(.fixed(50)),
    ]
    
    
    var body: some View {
        ScrollView(.vertical, showsIndicators: false, content: {
            ExtractedView()
            LazyVGrid(columns: columns, alignment: .center, spacing: nil, pinnedViews: [.sectionHeaders,.sectionFooters], content: {
                Section(header:
                            Text("Hello world")
                            .foregroundColor(Color.white)
                            .font(.title)
                            .padding(.horizontal)
                            .frame(maxWidth: .infinity)
                            .background(Color.blue)
                            .padding(),
                        footer:
                            Text("I Think you are Sramrt")
                            .font(.caption),
                        content: {
                            ForEach(0..<12) { index in
                                Rectangle()
                                    .fill(
                                        LinearGradient(gradient: Gradient(colors: [Color(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)), Color.blue]), startPoint: .topLeading, endPoint: .bottomTrailing))
                                    .frame(height: 250)
                            }
                        })
                Section(header:
                            Text("Hello Swift")
                            .foregroundColor(Color.white)
                            .font(.title)
                            .padding(.horizontal)
                            .frame(maxWidth: .infinity)
                            .background(Color.green)
                            .padding(),
                        footer:
                            Text("You can do it Bro 🐻")
                            .font(.caption),
                        content: {
                            ForEach(0..<12) { index in
                                Rectangle()
                                    .fill(
                                        LinearGradient(gradient: Gradient(colors: [Color(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)),Color.green]), startPoint: .topLeading, endPoint: .bottomTrailing))
                                    .frame(height: 250)
                            }
                        })
            })
        })
    }
}

struct GridBootcamp_Previews: PreviewProvider {
    static var previews: some View {
        GridBootcamp()
    }
}

struct ExtractedView: View {
    var body: some View {
        Rectangle()
            .fill(Color.white)
            .frame(height: 300)
            .overlay(
                HStack(spacing: nil, content: {
                    Circle()
                        .fill(Color.blue)
                        .frame(width: 100, height: 100)
                        .overlay(
                            Text("S Y")
                                .font(.system(size: 40))
                                .foregroundColor(.white)
                            ,alignment: .center
                        )
                        .overlay(
                            Circle()
                                .fill(Color.purple)
                                .frame(width: 35, height: 35)
                                .overlay(
                                    Text("22")
                                )
                                .shadow(color: .black.opacity(0.5), radius: 10, x: 0.0, y: 0.0)
                            ,alignment: .bottomTrailing
                        )
                    
                    VStack(alignment: .center, spacing: 20, content: {
                        Text("Sulaymon Yahyo !")
                            .font(.title)
                        Text("Allohga hamdlar bo`lsin Alhamdulillah ")
                            .font(.callout)
                    })
                })
            )
    }
}
