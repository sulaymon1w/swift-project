//
//  InitializerBootcamp.swift
//  lesson1
//
//  Created by Sulaymon on 23/08/21.
//

import SwiftUI

struct InitializerBootcamp: View {
    let backgroundColor: Color
    var count: Int
    let title: String
    
    init(count:Int,title:Mobile) {
        self.count=count
        
        if title==Mobile.appale {
            self.count=40
            
        }
        self.title="Trolllo"

        if count > 50 {
            self.backgroundColor=Color.red
        }else if count > 30{
            self.backgroundColor=Color.orange
        }else{
            self.backgroundColor=Color.purple
        }
    }
    
    enum Mobile {
        case appale
        case samsung
        case mi
        case Sulaymon
    }
    
    var body: some View {
        VStack(alignment: .center, spacing: 12, content: {
            Text("\(count)")
                .font(.largeTitle)
                .underline()
            Text(title)
                .font(.headline)
        })
        .foregroundColor(Color.white)
        .frame(width: 150, height: 150)
        .background(backgroundColor)
        .cornerRadius(10)
    }
}

struct InitializerBootcamp_Previews: PreviewProvider {
    static var previews: some View {
        HStack {
            InitializerBootcamp(count: 4, title: .Sulaymon)
            InitializerBootcamp(count: 33, title: .Sulaymon)
        }
    }
}
