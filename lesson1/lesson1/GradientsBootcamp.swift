//
//  GradientsBootcamp.swift
//  lesson1
//
//  Created by Sulaymon on 16/08/21.
//

import SwiftUI

struct GradientsBootcamp: View {
    var body: some View {
        RoundedRectangle(cornerRadius: 25.0)
            .fill(
//                Color.red
//                LinearGradient(
//                    gradient: Gradient(colors: [Color(#colorLiteral(red: 0.1215686277, green: 0.01176470611, blue: 0.4235294163, alpha: 1)), Color.blue,Color.red,Color.orange,Color.secondary]),
//                    startPoint: .topLeading,
//                    endPoint: .bottom)



//                RadialGradient(
//                    gradient: Gradient(colors: [Color(#colorLiteral(red: 0.9411764741, green: 0.4980392158, blue: 0.3529411852, alpha: 1)), Color(#colorLiteral(red: 0.1764705926, green: 0.01176470611, blue: 0.5607843399, alpha: 1)),Color(#colorLiteral(red: 0.1960784346, green: 0.3411764801, blue: 0.1019607857, alpha: 1))]),
//                    center: .topLeading,
//                    startRadius: /*@START_MENU_TOKEN@*/5/*@END_MENU_TOKEN@*/,
//                    endRadius: 300)
                
                AngularGradient(
                    gradient: Gradient(colors: [Color.red,Color.blue]),
                    center: .topLeading,
                    angle: .degrees(180+45))
                )
            .frame(width: 300, height: 200, alignment: .center)
    }
}

struct GradientsBootcamp_Previews: PreviewProvider {
    static var previews: some View {
        GradientsBootcamp()
    }
}
