//
//  SafeAreaBootcamp.swift
//  lesson1
//
//  Created by Sulaymon on 24/08/21.
//

import SwiftUI

struct SafeAreaBootcamp: View {
    
    let columns: [GridItem] = [
        GridItem(.flexible(),spacing: nil,alignment: nil),
    ]
    var body: some View {
        ZStack {
            Color.blue
                .ignoresSafeArea()
            ScrollView(.vertical, showsIndicators: false, content: {
                LazyVGrid(columns: columns, alignment: .center, spacing: nil, pinnedViews: [.sectionHeaders,.sectionFooters], content: {
                    Section(
                        header: Text("Sulaymon Yahyo")
                            .font(.largeTitle)
                            .bold()
                            .padding(.horizontal)
                            .frame(maxWidth:.infinity,alignment: .leading)
                            .background(/*@START_MENU_TOKEN@*//*@PLACEHOLDER=View@*/Color.blue/*@END_MENU_TOKEN@*/)
//                            .edgesIgnoringSafeArea(.top)
                            .ignoresSafeArea(.keyboard, edges: .top)
                        ,
                        content: {
                            ForEach(0..<10) { index in
                                RoundedRectangle(cornerRadius: 25.0)
                                    .fill(Color.white)
                                    .frame(height: 200, alignment: .center)
                                    .shadow(color: .black.opacity(0.5), radius: 10, x: 0.0, y: 0.0)
                                    .padding(20)
                            }
                        })

                })
            })
        }
    }
}

struct SafeAreaBootcamp_Previews: PreviewProvider {
    static var previews: some View {
        SafeAreaBootcamp()
    }
}
