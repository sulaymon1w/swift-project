//
//  ExtractSubviewsBootcamp.swift
//  lesson1
//
//  Created by Sulaymon on 26/08/21.
//

import SwiftUI

struct ExtractSubviewsBootcamp: View {
    var body: some View {
        ZStack {
            Color(#colorLiteral(red: 0.9411764741, green: 0.4980392158, blue: 0.3529411852, alpha: 1)).ignoresSafeArea(edges: .all)
            contentLayer
        }
    }
    
    var contentLayer: some View{
        HStack {
            MyItem(title: "Appless", count: 1, color: Color.red)
            MyItem(title: "Oranges", count: 10, color: Color.orange)
            MyItem(title: "Banana", count: 100, color: Color.purple)
        }
    }
}

struct ExtractSubviewsBootcamp_Previews: PreviewProvider {
    static var previews: some View {
        ExtractSubviewsBootcamp()
    }
}

struct MyItem: View {
    
    let title: String
    let count: Int
    let color: Color
    
    var body: some View {
        VStack {
            Text("\(count)")
            Text("\(title)")
        }
        .font(.largeTitle)
        .padding()
        .background(color)
        .cornerRadius(10)
    }
}
