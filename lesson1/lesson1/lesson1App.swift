//
//  lesson1App.swift
//  lesson1
//
//  Created by Sulaymon on 13/08/21.
//

import SwiftUI

@main
struct lesson1App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
