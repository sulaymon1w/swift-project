//
//  SwiftUIView.swift
//  lesson1
//
//  Created by Sulaymon on 23/08/21.
//

import SwiftUI

struct SwiftUIView: View {
    
    let data:[String]=["hi","hey siri","hey Sulaymon Yahyo","Akajon yaxshimisiz ?"]
    let myString:String = "hello"
    
    var body: some View {
        VStack {
            ForEach(data.indices) { index in
                Text("\(data[index]): \(index)")
            }
        }
        //        VStack {
        //            ForEach(0..<10) { index in
        //                HStack {
        //                    Circle()
        //                        .frame(width: 50, height: 50)
        //                        .overlay(
        //                            Text("\(index+1)")
        //                            .foregroundColor(.white))
        //                    Text("This is index = \(index)")
        //                }
        //            }
        //        }
    }
}

struct SwiftUIView_Previews: PreviewProvider {
    static var previews: some View {
        SwiftUIView()
    }
}
