//
//  IconBootcamp.swift
//  lesson1
//
//  Created by Sulaymon on 16/08/21.
//

import SwiftUI

struct IconBootcamp: View {
    var body: some View {
        Image(systemName: "touchid")
//            .font(.system(size: 200))
            .renderingMode(.original)
            .resizable()
//            .aspectRatio(contentMode: .fit)
            .scaledToFit()
//            .scaledToFill()
//             .foregroundColor(Color(#colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)))
            .frame(width: 300, height: 300)
//            .clipped()
    }
}

struct IconBootcamp_Previews: PreviewProvider {
    static var previews: some View {
        IconBootcamp()
    }
}
