//
//  FrameBootcamp.swift
//  lesson1
//
//  Created by Sulaymon on 16/08/21.
//

import SwiftUI

struct FrameBootcamp: View {
    var body: some View {
        Text("Hello, World!")
            .background(Color(#colorLiteral(red: 0.5725490451, green: 0, blue: 0.2313725501, alpha: 1)))
            .frame(height:100, alignment: .top)
            .background(Color.red)
            .frame(width:150)
            .background(Color.yellow)
            .frame(maxWidth:.infinity,alignment: .leading)
            .background(Color.yellow)
            .frame(height:400)
            .background(Color.yellow)
            .frame(maxHeight:.infinity, alignment: .leading)
            .background(Color.green)
    }
}

struct FrameBootcamp_Previews: PreviewProvider {
    static var previews: some View {
        FrameBootcamp()	
    }
}
