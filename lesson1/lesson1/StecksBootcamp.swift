//
//  StecksBootcamp.swift
//  lesson1
//
//  Created by Sulaymon on 19/08/21.
//

import SwiftUI

struct StecksBootcamp: View {
    var body: some View {
        VStack(alignment: .center, spacing: 50, content: {
        
            ZStack {
                Circle()
                    .frame(width: 100, height: 100)
                Text("1")
                    .font(.title)
                    .foregroundColor(.white)
            }
            
            Text("1")
                .font(.title)
                .foregroundColor(.white)
                .background(
                    Circle()
                        .frame(width: 100, height: 100)
                )
        })
//        VStack(alignment: .leading, spacing: 10, content: {
//            Rectangle()
//                .fill(Color.red)
//                .frame(width: 100, height: 100)
//            Rectangle()
//                .fill(Color.green)
//                .frame(width: 100, height: 100)
//            Rectangle()
//                .fill(Color.yellow)
//                .frame(width: 100, height: 100)
//        })
    }
}

struct StecksBootcamp_Previews: PreviewProvider {
    static var previews: some View {
        StecksBootcamp()
    }
}
