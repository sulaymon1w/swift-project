//
//  text.swift
//  lesson1
//
//  Created by Sulaymon on 13/08/21.
//

import SwiftUI

struct text: View {
    var body: some View {
        Text("Hello world Sulaymon Yahyo yaxshimsiz aka uydagilariz yaxshimi ? Auuuu Brodari aziz nimagala bovottimi eeee".capitalized)
//            .font(.title)
//            .bold()
//            .italic()
//            .underline(true, color: Color.red)
//            .fontWeight(.heavy)
//            .strikethrough(true, color: Color.red)
//            .font(.system(size: 25, weight: .bold, design: .serif))
//            .baselineOffset(10.0)
//            .kerning(1.0)
            .foregroundColor(Color.red)
            .multilineTextAlignment(.leading)
            .frame(width:200, height: 100, alignment: .center)
            .minimumScaleFactor(1.0)
    }
}

struct text_Previews: PreviewProvider {
    static var previews: some View {
        text()
    }
}
