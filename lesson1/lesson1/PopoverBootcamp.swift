//
//  PopoverBootcamp.swift
//  lesson1
//
//  Created by Sulaymon on 29/08/21.
//

import SwiftUI

struct PopoverBootcamp: View {
    var body: some View {
        Text("Hello, World!")
    }
}

struct PopoverBootcamp_Previews: PreviewProvider {
    static var previews: some View {
        PopoverBootcamp()
    }
}
