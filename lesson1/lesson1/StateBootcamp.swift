//
//  StateBootcamp.swift
//  lesson1
//
//  Created by Sulaymon on 25/08/21.
//

import SwiftUI

struct StateBootcamp: View {
    
    @State var CorgroundColorVar:Color=Color.gray;
    @State var MyTitle:String="This is Title";
    @State var count:Int8=0
    
    var body: some View {
        ZStack {
            CorgroundColorVar
                .ignoresSafeArea(edges: .all)
            VStack(spacing:20) {
                Text(MyTitle)
                    .font(.title)
                    .fontWeight(.bold)
                Text("Count: \(count)")
                    .fontWeight(.bold)
                    .underline()
                
                HStack(spacing:20) {
                    Button("Button 1") {
                        CorgroundColorVar=Color.red;
                        MyTitle="Title title";
                        count+=1
                    }
                    Button("Button 2") {
                        CorgroundColorVar=Color.purple;
                        MyTitle="Title 22";
                        count-=1
                    }
                }
            }
        }
        .foregroundColor(Color.white)
    }
}

struct StateBootcamp_Previews: PreviewProvider {
    static var previews: some View {
        StateBootcamp()
    }
}
