//
//  ExtracedFuctionsBootcamp.swift
//  lesson1
//
//  Created by Sulaymon on 25/08/21.
//

import SwiftUI

struct ExtracedFuctionsBootcamp: View {
    
    @State var bacgroundColorVar:Color=Color.pink
    
    func buttonPressed() {
        bacgroundColorVar=Color.yellow
    }
    
    var body: some View {
        ZStack {
            bacgroundColorVar.ignoresSafeArea(edges: .all)
            contentLayer
        }
    }
    
    var contentLayer: some View {
        VStack {
            Text("Hello, World!")
                .font(.largeTitle)
            Button(action: {
                buttonPressed()
            }, label: {
                Text("Button")
                    .font(.title)
                    .padding()
                    .foregroundColor(Color.white)
                    .background(Color.black)
                    .cornerRadius(10)
            })
        }
    }
}

struct ExtracedFuctionsBootcamp_Previews: PreviewProvider {
    static var previews: some View {
        ExtracedFuctionsBootcamp()
    }
}
