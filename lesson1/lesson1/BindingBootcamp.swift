//
//  BindingBootcamp.swift
//  lesson1
//
//  Created by Sulaymon on 26/08/21.
//

import SwiftUI

struct BindingBootcamp: View {
    
    @State var backgroundColor: Color=Color.green
    @State var title:String = "Title"
    
    var body: some View {
        ZStack {
            backgroundColor.ignoresSafeArea(edges: .all)
            VStack {
                Text(title)
                    .foregroundColor(Color.white)
                MyButton(backgroundColor: $backgroundColor, title: $title)
            }
        }
    }
}

struct BindingBootcamp_Previews: PreviewProvider {
    static var previews: some View {
        BindingBootcamp()
    }
}

struct MyButton: View {
    
    @Binding var backgroundColor:Color
    @Binding var title:String
    
    @State var buttonColor:Color=Color.red
    
    var body: some View {
        Button(action: {
            backgroundColor=Color.orange
            buttonColor=Color.pink
            title="new title tadam )))"
        }, label: {
            Text("Button")
                .foregroundColor(Color.white)
                .padding()
                .padding(.horizontal)
                .background(buttonColor)
                .cornerRadius(15)
        })
    }
}
