//
//  ImageBootcamp.swift
//  lesson1
//
//  Created by Sulaymon on 16/08/21.
//

import SwiftUI

struct ImageBootcamp: View {
    var body: some View {
        Image("firstImg")
//            .renderingMode(/*@START_MENU_TOKEN@*/.template/*@END_MENU_TOKEN@*/)
            .resizable()
            .scaledToFill()
//            .foregroundColor(.blue)
            .frame(width: 300, height: 300)
//            .clipped()
//            .cornerRadius(150)
//            .clipShape(
//                Circle()
//                Ellipse()
//                RoundedRectangle(cornerRadius: /*@START_MENU_TOKEN@*/25.0/*@END_MENU_TOKEN@*/)
//            )
    }
}

struct ImageBootcamp_Previews: PreviewProvider {
    static var previews: some View {
        ImageBootcamp()
    }
}
