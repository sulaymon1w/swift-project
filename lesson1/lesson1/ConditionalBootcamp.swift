//
//  ConditionalBootcamp.swift
//  lesson1
//
//  Created by Sulaymon on 26/08/21.
//

import SwiftUI

struct ConditionalBootcamp: View {
    @State var showCircle:Bool=false
    @State var showRectangle:Bool=false
    @State var loading:Bool=true
    var body: some View {
        VStack(spacing: 20) {
            
            Button("\(loading ? "Loading..." : "OK")") {
                loading.toggle()
            }
            
            if loading{
                ProgressView()
            }
            
            
            //
            //            Button("Circle Button \(showCircle ? "korindi":"ko`rinmadi")") {
            //                showCircle.toggle()
            //            }
            //            Button("Rectangle Button: \(showRectangle.description)") {
            //                showRectangle.toggle()
            //            }
            //
            //            if showCircle {
            //                Circle()
            //                    .frame(width: 100, height: 100, alignment: .center)
            //            }else if showRectangle{
            //                Rectangle()
            //                    .frame(width: 100, height: 100, alignment: .center)
            //            }else{
            //                RoundedRectangle(cornerRadius: 25.0)
            //                    .frame(width:200, height: 100, alignment: .center)
            //            }
            Spacer()
        }
    }
}

struct ConditionalBootcamp_Previews: PreviewProvider {
    static var previews: some View {
        ConditionalBootcamp()
    }
}
