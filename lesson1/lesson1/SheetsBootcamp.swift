//
//  SheetsBootcamp.swift
//  lesson1
//
//  Created by Sulaymon on 29/08/21.
//

import SwiftUI

struct SheetsBootcamp: View {
    
    @State var showSheet: Bool = false
    
    var body: some View {
        ZStack {
            Color.green.ignoresSafeArea(edges: .all)
            Button(action: {
                showSheet.toggle()
            }, label: {
                Text("Button")
                    .foregroundColor(Color.green)
                    .font(.headline)
                    .padding()
                    .background(Color.white.cornerRadius(50))
            })
            .fullScreenCover(isPresented: $showSheet, content: {
                SecountSheetsBootcamp()
            })
            .sheet(isPresented: $showSheet, content: {
                SecountSheetsBootcamp()
//                SecountSheetsBootcamp(showSheet: $showSheet)
            })
        }
    }
}

struct SecountSheetsBootcamp: View {
    
    @Environment(\.presentationMode) var presentationMode;
    
    var body: some View {
        ZStack(alignment: .topLeading) {
            Color.red.ignoresSafeArea(edges: .all)
            Button(action: {
                presentationMode.wrappedValue.dismiss()
            }, label: {
                Image(systemName: "xmark")
                    .foregroundColor(Color.white)
                    .font(.largeTitle)
                    .padding()
            })
        }
    }
}

struct SheetsBootcamp_Previews: PreviewProvider {
    static var previews: some View {
        SheetsBootcamp()
    }
}
