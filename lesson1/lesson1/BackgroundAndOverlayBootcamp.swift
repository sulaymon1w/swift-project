//
//  BackgroundAndOverlayBootcamp.swift
//  lesson1
//
//  Created by Sulaymon on 16/08/21.
//

import SwiftUI

struct BackgroundAndOverlayBootcamp: View {
    var body: some View {
        Image(systemName: "heart.fill")
            .font(.system(size: 40))
            .foregroundColor(Color.white)
            .background(
                Circle()
                    .fill(
                        LinearGradient(gradient: Gradient(colors: [Color(#colorLiteral(red: 0.5568627715, green: 0.3529411852, blue: 0.9686274529, alpha: 1)), Color(#colorLiteral(red: 0.2196078449, green: 0.007843137719, blue: 0.8549019694, alpha: 1))]), startPoint: .topLeading, endPoint: .bottomTrailing)
                    )
                    .frame(width: 100, height: 100)
                    .shadow(color: Color(#colorLiteral(red: 0.01680417731, green: 0.1983509958, blue: 1, alpha: 0.5855362538)), radius: 10, x: 10.0, y: 10)
                    .overlay(
                        Circle()
                            .fill(Color.blue)
                            .frame(width: 35, height: 35)
                            .overlay(
                                Text("1")
                                    .font(.headline)
                                    .foregroundColor(Color.white)
                            )
                            .shadow(color: Color(#colorLiteral(red: 0.01680417731, green: 0.1983509958, blue: 1, alpha: 0.5855362538)), radius: 10, x: 10.0, y: 10)
                        ,alignment: .bottomTrailing
                    )
            )
    }
}

struct BackgroundAndOverlayBootcamp_Previews: PreviewProvider {
    static var previews: some View {
        BackgroundAndOverlayBootcamp()
    }
}
