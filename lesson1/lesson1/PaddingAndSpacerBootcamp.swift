//
//  PaddingAndSpacerBootcamp.swift
//  lesson1
//
//  Created by Sulaymon on 20/08/21.
//

import SwiftUI

struct PaddingAndSpacerBootcamp: View {
    var body: some View {
        VStack(alignment: .leading, spacing: 10, content: {
            let extractedExpr = Text("Hello, World!")
            extractedExpr
                .font(.largeTitle)
                .fontWeight(.semibold)
                .frame(maxWidth: .infinity, alignment: .leading)
                .padding(.leading,20)
            Text("Hello word My name is Sulaymon Yahyo and I am Middle developer and I know many coding languages for exaple Java, Javacript, Swift, React, Dart, Python, Node js")
                .padding(.leading,3)
//                .multilineTextAlignment(.center)
        })
        .padding(.vertical, 40)
        .background(/*@START_MENU_TOKEN@*//*@PLACEHOLDER=View@*/Color.blue/*@END_MENU_TOKEN@*/)
        .padding()
        .background(Color.red.shadow(color: Color.black.opacity(0.3), radius: /*@START_MENU_TOKEN@*/10/*@END_MENU_TOKEN@*/, x: /*@START_MENU_TOKEN@*/0.0/*@END_MENU_TOKEN@*/, y: 10))
        .padding(.horizontal)
        
//            .background(Color.yellow)
////            .padding()
//            .padding(.horizontal,10)
//            .padding(.leading,20)
//            .background(Color(#colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)))
    }
}

struct PaddingAndSpacerBootcamp_Previews: PreviewProvider {
    static var previews: some View {
        PaddingAndSpacerBootcamp()
    }
}
