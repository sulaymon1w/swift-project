//
//  TernaryBootcamp.swift
//  lesson1
//
//  Created by Sulaymon on 29/08/21.
//

import SwiftUI

struct TernaryBootcamp: View {
    
    @State var isStartingState:Bool=false
    
    var body: some View {
        VStack {
            Text(isStartingState ? "nimadir":"nimadir 2")
            
            RoundedRectangle(cornerRadius: isStartingState ? 25.0 : 0)
                .fill(isStartingState ? Color.red : Color.blue)
                .frame(width: isStartingState ? 400 : 200, height: 100)
                .overlay(
                    Button(action: {
                        isStartingState.toggle()
                    }, label: {
                        Text("Color: \(isStartingState ? "Red":"Blue")")
                            .foregroundColor(isStartingState ? Color.blue : Color.red)
                            .fontWeight(.bold)
                            .font(.title)
                    })
                )
            Spacer()
        }
    }
}

struct TernaryBootcamp_Previews: PreviewProvider {
    static var previews: some View {
        TernaryBootcamp()
    }
}
