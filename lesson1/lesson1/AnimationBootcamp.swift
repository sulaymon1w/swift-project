//
//  AnimationBootcamp.swift
//  lesson1
//
//  Created by Sulaymon on 29/08/21.
//

import SwiftUI

struct AnimationBootcamp: View {
    
    @State var isAnimcated:Bool=false
    
    var body: some View {
        VStack {
            Button("Button")
            {
//                withAnimation(
////                    Animation.default.delay(2.0)
//                    Animation.default
////                        .repeatCount(5,autoreverses: false)
//                        .repeatForever(autoreverses: false)
//                ){
                    isAnimcated.toggle()
//                }
            }
            Spacer()
            RoundedRectangle(cornerRadius: isAnimcated ? 50 : 25)
                .fill(isAnimcated ? Color.red : Color.green)
                .frame(
                    width: isAnimcated ? 100 : 300,
                    height: isAnimcated ? 100 : 300
                )
                .rotationEffect(Angle(degrees: isAnimcated ? 360 : 0))
                .offset(y: isAnimcated ? 300 :0)
            Spacer()
         }
        .animation(Animation.default
                    .repeatForever(autoreverses: true))
    }
}

struct AnimationBootcamp_Previews: PreviewProvider {
    static var previews: some View {
        AnimationBootcamp()
    }
}
