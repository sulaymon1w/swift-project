//
//  TransitionBootcamp.swift
//  lesson1
//
//  Created by Sulaymon on 29/08/21.
//

import SwiftUI

struct TransitionBootcamp: View {
    
    @State var showView:Bool=false;
    
    var body: some View {
        ZStack(alignment: .bottom) {
            VStack {
                Button("Button") {
                    showView.toggle()
                }
                Spacer()
            }
            if showView {
                RoundedRectangle(cornerRadius: 30.0)
                    .frame(height: UIScreen.main.bounds.height * 0.5)
                    .transition(.asymmetric(
                                    insertion: .move(edge: .leading),
                                    removal: AnyTransition.opacity.animation(.easeInOut))
                    )
//                    .transition(AnyTransition.opacity.animation(.easeInOut))
//                    .transition(AnyTransition.scale.animation(.easeInOut))
//                    .transition(.move(edge: .bottom))
//                    .animation(.spring(response: 0.5, dampingFraction: 0.2, blendDuration: 10))
                    .animation(.easeInOut)
            }
        }
        .ignoresSafeArea(edges: .bottom)
    }
}

struct TransitionBootcamp_Previews: PreviewProvider {
    static var previews: some View {
        TransitionBootcamp()
    }
}
