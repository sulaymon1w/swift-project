//
//  SpacerBootcamp.swift
//  lesson1
//
//  Created by Sulaymon on 22/08/21.
//

import SwiftUI

struct SpacerBootcamp: View {
    var body: some View {
        VStack {
            HStack(alignment: .center, spacing: 0, content: {
                Image(systemName: "xmark")
                Spacer()
                Image(systemName: "gear")
            })
            .font(.title)
//            .background(Color.yellow)
            .padding(.horizontal,20)
//            .background(/*@START_MENU_TOKEN@*//*@PLACEHOLDER=View@*/Color.blue/*@END_MENU_TOKEN@*/)
            Spacer()
        }
    }
}

struct SpacerBootcamp_Previews: PreviewProvider {
    static var previews: some View {
        SpacerBootcamp()
    }
}
