//
//  ButtonBootcamp.swift
//  lesson1
//
//  Created by Sulaymon on 24/08/21.
//

import SwiftUI

struct ButtonBootcamp: View {
    
    @State var title: String="This is test bro"
    
    var body: some View {
        VStack {
            Text(title)
            Button("Click me") {
                self.title="Trollolllo"
            }.accentColor(Color.red)
            Button(action: {
                self.title="Trolololo 2"
            }, label: {
                Text("Button")
                    .font(.title)
                    .padding()
                    .padding(.horizontal,30)
                    .foregroundColor(Color.white)
                    .background(Color.red.cornerRadius(20))
            })
            Button(action: {
                self.title="Trolollo"
            }, label: {
                Circle()
                    .fill(Color.white)
                    .frame(width: 100, height: 100, alignment: .center)
                    .shadow(radius: 10)
                    .overlay(
                        Image(systemName: "heart.fill")
                            .font(.largeTitle)
                            .foregroundColor(Color(#colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)))
                    )
            })
            Button(action: {
                self.title="TRollololasdflo"
            }, label: {
                Text("Button")
                    .font(.caption)
                    .fontWeight(.bold)
                    .foregroundColor(Color.red)
                    .padding()
                    .padding(.horizontal)
                    .overlay(
                        Capsule()
                            .stroke(Color.red,lineWidth: 3.0)
                    )
            })
        }
    }
}

struct ButtonBootcamp_Previews: PreviewProvider {
    static var previews: some View {
        ButtonBootcamp()
    }
}
