//
//  ShapesBootcamp.swift
//  lesson1
//
//  Created by Sulaymon on 13/08/21.
//

import SwiftUI

struct ShapesBootcamp: View {
    var body: some View {
//        Ellipse()
//        Capsule(style: .continuous)
//        Rectangle()
        RoundedRectangle(cornerRadius: 25.0)
            .trim(from: 0.4, to: 1.0)
            .frame(width: 200, height: 100)
//        ZStack {
//            Circle()
//    //            .stroke(Color.red)
//    //            .stroke(Color.green,lineWidth: 30)
//    //            .foregroundColor(.blue)
//    //            .fill(Color.blue)
//    //            .stroke(Color.orange,style: StrokeStyle(lineWidth: 10, lineCap: .butt, dash: [30]))
//
//                .trim(from: 0.2, to: 1.0)
//                .stroke(Color.purple,lineWidth: 50)
//                .frame(width: 400, height: 300, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
//            Text("99 %")
//                            .font(.system(size: 90, weight: .bold, design: .default))
//                .fontWeight(/*@START_MENU_TOKEN@*/.bold/*@END_MENU_TOKEN@*/)
//
//        }
    }
}

struct ShapesBootcamp_Previews: PreviewProvider {
    static var previews: some View {
        ShapesBootcamp()
    }
}
