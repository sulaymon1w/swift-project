//
//  AnimationTimingBootcamp.swift
//  lesson1
//
//  Created by Sulaymon on 29/08/21.
//

import SwiftUI

struct AnimationTimingBootcamp: View {
    
    @State var isAnimation: Bool = false

    var body: some View {
        VStack {
            Button("Button") {
                isAnimation.toggle()
            }
                        RoundedRectangle(cornerRadius: 20)
                            .fill(Color.blue)
                            .frame(width: isAnimation ? 350 : 50, height: 100, alignment: .center)
                            .animation(Animation.spring(response: 0.5, dampingFraction: 0.2, blendDuration: 1.0))

//            RoundedRectangle(cornerRadius: 20)
//                .fill(Color.blue)
//                .frame(width: isAnimation ? 350 : 50, height: 100, alignment: .center)
//                .animation(Animation.linear(duration: 10))
//
//            RoundedRectangle(cornerRadius: 20)
//                .fill(Color.blue)
//                .frame(width: isAnimation ? 350 : 50, height: 100, alignment: .center)
//                .animation(Animation.easeIn(duration: 10))
//
//            RoundedRectangle(cornerRadius: 20)
//                .fill(Color.blue)
//                .frame(width: isAnimation ? 350 : 50, height: 100, alignment: .center)
//                .animation(Animation.easeInOut(duration: 10))
//
//            RoundedRectangle(cornerRadius: 20)
//                .fill(Color.blue)
//                .frame(width: isAnimation ? 350 : 50, height: 100, alignment: .center)
//                .animation(Animation.easeOut(duration: 10))
            
        }
    }
}

struct AnimationTimingBootcamp_Previews: PreviewProvider {
    static var previews: some View {
        AnimationTimingBootcamp()
    }
}
